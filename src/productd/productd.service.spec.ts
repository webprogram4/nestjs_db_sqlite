import { Test, TestingModule } from '@nestjs/testing';
import { ProductdService } from './productd.service';

describe('ProductdService', () => {
  let service: ProductdService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProductdService],
    }).compile();

    service = module.get<ProductdService>(ProductdService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
