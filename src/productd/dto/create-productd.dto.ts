import { IsNotEmpty, Length, IsInt } from 'class-validator';

export class CreateProductdDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  @IsInt()
  price: number;
}
