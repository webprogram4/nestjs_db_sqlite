import { PartialType } from '@nestjs/mapped-types';
import { CreateProductdDto } from './create-productd.dto';

export class UpdateProductdDto extends PartialType(CreateProductdDto) {}
