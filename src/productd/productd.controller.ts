import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ProductdService } from './productd.service';
import { CreateProductdDto } from './dto/create-productd.dto';
import { UpdateProductdDto } from './dto/update-productd.dto';

@Controller('productd')
export class ProductdController {
  constructor(private readonly productdService: ProductdService) {}

  @Post()
  create(@Body() createProductdDto: CreateProductdDto) {
    return this.productdService.create(createProductdDto);
  }

  @Get()
  findAll() {
    return this.productdService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.productdService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateProductdDto: UpdateProductdDto,
  ) {
    return this.productdService.update(+id, updateProductdDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productdService.remove(+id);
  }
}
