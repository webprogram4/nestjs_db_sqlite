import { Module } from '@nestjs/common';
import { ProductdService } from './productd.service';
import { ProductdController } from './productd.controller';

@Module({
  controllers: [ProductdController],
  providers: [ProductdService],
})
export class ProductdModule {}
