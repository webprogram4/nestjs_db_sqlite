import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
@Entity()
export class Productd {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  name: string;
  @Column()
  price: number;
}
