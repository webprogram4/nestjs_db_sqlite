import { Test, TestingModule } from '@nestjs/testing';
import { ProductdController } from './productd.controller';
import { ProductdService } from './productd.service';

describe('ProductdController', () => {
  let controller: ProductdController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductdController],
      providers: [ProductdService],
    }).compile();

    controller = module.get<ProductdController>(ProductdController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
