import { Injectable } from '@nestjs/common';
import { CreateProductdDto } from './dto/create-productd.dto';
import { UpdateProductdDto } from './dto/update-productd.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Productd } from './entities/productd.entity';

@Injectable()
export class ProductdService {
  constructor(
    @InjectRepository(Productd)
    private productdRepository: Repository<Productd>,
  ) {}
  create(createProductDto: CreateProductdDto) {
    const product: Productd = new Productd();
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    return this.productdRepository.save(product);
  }

  findAll(): Promise<Productd[]> {
    return this.productdRepository.find();
  }

  findOne(id: number) {
    return this.productdRepository.findOneBy({ id: id });
  }

  async update(id: number, updateProductDto: UpdateProductdDto) {
    const product = await this.productdRepository.findOneBy({ id: id });
    const updatedProductd = { ...product, ...updateProductDto };
    return this.productdRepository.save(updatedProductd);
  }

  async remove(id: number) {
    const product = await this.productdRepository.findOneBy({ id: id });
    return this.productdRepository.remove(product);
  }
}
